

														============
														   HR BOT
														============

														
								/**
								 * @license Copyright (c) 2018, ERA-INFOTECH LIMITED. All rights reserved.
								 */											



# Dialogflow (Api.ai) - sample webhook implementation in Python Flask.

# This is a really simple webhook implementation that gets Dialogflow classification JSON (i.e. a JSON output of Dialogflow /query endpoint) 
and returns a fulfillment response.

# Using this powerful web service bot can respond to users collecting information from external web services and even Databases!!

More info about Dialogflow webhooks could be found here:
[Dialogflow Webhook](https://dialogflow.com/docs/fulfillment#webhook)

# Deploy to:
[![Deploy to Heroku](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy)

# What does the service do?

Using this service chatBot could simulate basic tasks like Leave Balance View, Leave Application, Movement Register, ACR information on behalf of the HR. Employees could
get this basic HR related services through chatting or talking with the BOT. This service also has an extended functionality of Leave Reporting and end to end approvals facilities.

So, HR BOT actually works like a wrapper over the HR Department of a company providing most common and frequently asked questions and services to the employees.

HR BOT has been intregated with the HR & Payroll Software developed by ERA-INFOTECH LIMITED. With this extension with the HR & Payroll Software employees just have 
to interact with the bot to get the most frequent automated services of HR in return.

Regarding three tier architecture this application actually works as a loosely coupled middleware and maintains interaction with Dialogflow using Webhook Protocol and 
communicate RESTFUL Web Services with python Request-Response mechanism. All these three tier applications have to work harmonically for completing a successfull response 
to a user.  

Example:

			User: Show me my leave balance
			BOT: Please mention leave category (CL/EL/ML/LFA/ALL)
			User: CL
			BOT: Your leave balance for CL is: 13. Thanks!! 

#Features:

	1. Extended Database interaction capability.
	2. Fully supported with Python Request-Response mechanism.
	3. Could respond to multiple intents based on action parameters.
	4. Able to receive request parameters as JSON and prepare response for user intelligently. 
	5. Extract parametric values from user to generate sql query using different algoritms independently. 
	6. Nested requested could be invoked to interact external webservices as well as RESTFUL Web Services.
	7. User Authentication support added.
	8. SSL (Secured Socket Layer) included.
	
	
Regards, 

Developer : Salman Rakin
Project Manager: Anwar Hossain

Artificial Intelligence Team
ERA-INFOTECH LIMITED.
	

